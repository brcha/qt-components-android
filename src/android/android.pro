include (../../qt-components.pri)

TARGETPATH = com/nokia/android.1.1
TEMPLATE = lib
TARGET = $$qtLibraryTarget(androidplugin_1_1_2)
INCLUDEPATH += $$PWD $$PWD/indicators

win32|mac:!wince*:!win32-msvc:!macx-xcode:CONFIG += debug_and_release build_all
CONFIG += qt plugin copy_native install_native
QT += declarative svg
mobility {
    MOBILITY += feedback systeminfo
    QT += network
}

SOURCES += \
    plugin.cpp \
    adeclarative.cpp \
    adeclarativefocusscopeitem.cpp \
    adeclarativeicon.cpp \
    adeclarativeimageprovider.cpp \
    adeclarativeimplicitsizeitem.cpp \
    adeclarativeinputcontext.cpp \
    adeclarativemagnifier.cpp \
    adeclarativemaskedimage.cpp \
    adeclarativescreen.cpp \
    adeclarativescreen_p.cpp \
    adeclarativescreen_p_resize.cpp \
    adeclarativescreen_p_sensor.cpp \
    adeclarativestyle.cpp \
    adeclarativestyleinternal.cpp \
    aiconpool.cpp \
    amousegrabdisabler.cpp \
    apopupmanager.cpp \
    astyleengine.cpp \
    astylefactory.cpp \
    indicators/adeclarativeindicatorcontainer.cpp \
    asnapshot.cpp \
    adeclarativeinputcontext_p.cpp

HEADERS += \
    adeclarative.h \
    adeclarativefocusscopeitem.h \
    adeclarativeicon.h \
    adeclarativeimageprovider.h \
    adeclarativeimplicitsizeitem.h \
    adeclarativeinputcontext.h \
    adeclarativemagnifier.h \
    adeclarativemaskedimage.h \
    adeclarativemaskedimage_p.h \
    adeclarativescreen.h \
    adeclarativescreen_p.h \
    adeclarativescreen_p_resize.h \
    adeclarativescreen_p_sensor.h \
    adeclarativestyle.h \
    adeclarativestyleinternal.h \
    aiconpool.h \
    amousegrabdisabler.h \
    apopupmanager.h \
    astyleengine.h \
    astylefactory.h \
    indicators/adeclarativeindicatorcontainer.h \
    asnapshot.h \
    adeclarativeinputcontext_p.h

RESOURCES += \
    android.qrc

QML_FILES = \
    qmldir \
    ApplicationWindow.qml \
    AppManager.js \
    Button.qml \
    ButtonColumn.qml \
    ButtonGroup.js \
    ButtonRow.qml \
    BusyIndicator.qml \
    CheckBox.qml \
    CommonDialog.qml \
    ContextMenu.qml \
    Dialog.qml \
    Fader.qml \
    Label.qml \
    ListHeading.qml \
    ListItem.qml \
    ListItemText.qml \
    Menu.qml \
    MenuContent.qml \
    MenuItem.qml \
    MenuLayout.qml \
    Page.qml \
    PageStack.js \
    PageStack.qml \
    PageStackWindow.qml \
    Popup.qml \
    ProgressBar.qml \
    QueryDialog.qml \
    RadioButton.qml \
    RectUtils.js \
    ScrollBar.qml \
    ScrollDecorator.qml \
    SectionScroller.js \
    SectionScroller.qml \
    SelectionDialog.qml \
    SelectionListItem.qml \
    Slider.qml \
    Switch.qml \
    TabBar.qml \
    TabBarLayout.qml \
    TabButton.qml \
    TabGroup.js \
    TabGroup.qml \
    TextArea.qml \
    TextField.qml \
    TextMagnifier.qml \
    TextContextMenu.qml \
    TextSelectionHandle.qml \
    TextTouchController.qml \
    TextTouchTools.qml \
    ToolBar.qml \
    ToolBarLayout.qml \
    ToolButton.qml \
    ToolTip.qml \
    Window.qml

win32: LIBS += -lpsapi # for allocated memory info

include(../../qml.pri)




