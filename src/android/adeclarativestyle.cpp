/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Components project.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

#define QT_NO_CAST_FROM_ASCII
#define QT_NO_CAST_TO_ASCII

#include "adeclarativestyle.h"
#include "astyleengine.h"
#include "adeclarative.h"
#include "adeclarativescreen.h"

#include <QObject>

class ADeclarativeStylePrivate
{
public:
    ADeclarativeStylePrivate() {}

    AStyleEngine *engine;
};

ADeclarativeStyle::ADeclarativeStyle(AStyleEngine *engine, QObject *parent)
    : QObject(parent),
      d_ptr(new ADeclarativeStylePrivate())
{
    Q_D(ADeclarativeStyle);
    d->engine = engine;
    QObject::connect(engine, SIGNAL(fontParametersChanged()), this, SIGNAL(fontParametersChanged()));
    QObject::connect(engine, SIGNAL(layoutParametersChanged()), this, SIGNAL(layoutParametersChanged()));
    QObject::connect(engine, SIGNAL(colorParametersChanged()), this, SIGNAL(colorParametersChanged()));
}

ADeclarativeStyle::~ADeclarativeStyle()
{
}

QString ADeclarativeStyle::fontFamilyRegular() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->fontFamilyParameter(QLatin1String("font-family-regular"));
}

int ADeclarativeStyle::fontSizeLarge() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->layoutParameter(QLatin1String("font-size-large"));
}

int ADeclarativeStyle::fontSizeMedium() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->layoutParameter(QLatin1String("font-size-medium"));
}

int ADeclarativeStyle::fontSizeSmall() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->layoutParameter(QLatin1String("font-size-small"));
}

int ADeclarativeStyle::graphicSizeLarge() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->layoutParameter(QLatin1String("graphic-size-large"));
}

int ADeclarativeStyle::graphicSizeMedium() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->layoutParameter(QLatin1String("graphic-size-medium"));
}

int ADeclarativeStyle::graphicSizeSmall() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->layoutParameter(QLatin1String("graphic-size-small"));
}

int ADeclarativeStyle::graphicSizeTiny() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->layoutParameter(QLatin1String("graphic-size-tiny"));
}

int ADeclarativeStyle::paddingLarge() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->layoutParameter(QLatin1String("padding-large"));
}

int ADeclarativeStyle::paddingMedium() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->layoutParameter(QLatin1String("padding-medium"));
}

int ADeclarativeStyle::paddingSmall() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->layoutParameter(QLatin1String("padding-small"));
}

int ADeclarativeStyle::borderSizeMedium() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->layoutParameter(QLatin1String("border-size-medium"));
}

QColor ADeclarativeStyle::colorBackground() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-background"));
}

QColor ADeclarativeStyle::colorNormalLight() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-normal-light"));
}

QColor ADeclarativeStyle::colorNormalMid() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-normal-mid"));
}

QColor ADeclarativeStyle::colorNormalDark() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-normal-dark"));
}

QColor ADeclarativeStyle::colorNormalLink() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-normal-link"));
}

QColor ADeclarativeStyle::colorPressed() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-pressed"));
}

QColor ADeclarativeStyle::colorChecked() const
{
    qDebug() << "warning: ADeclarativeStyle::colorChecked() is deprecated, use colorLatched() instead";
    return colorLatched();
}

QColor ADeclarativeStyle::colorLatched() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-latched"));
}

QColor ADeclarativeStyle::colorHighlighted() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-highlighted"));
}

QColor ADeclarativeStyle::colorDisabledLight() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-disabled-light"));
}

QColor ADeclarativeStyle::colorDisabledMid() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-disabled-mid"));
}

QColor ADeclarativeStyle::colorDisabledDark() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-disabled-dark"));
}

QColor ADeclarativeStyle::colorTextSelection() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-text-selection"));
}

QColor ADeclarativeStyle::colorBackgroundInverted() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-background-inverse"));
}

QColor ADeclarativeStyle::colorNormalLightInverted() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-normal-light-inverse"));
}

QColor ADeclarativeStyle::colorNormalMidInverted() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-normal-mid-inverse"));
}

QColor ADeclarativeStyle::colorNormalDarkInverted() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-normal-dark-inverse"));
}

QColor ADeclarativeStyle::colorNormalLinkInverted() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-normal-link-inverse"));
}

QColor ADeclarativeStyle::colorPressedInverted() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-pressed-inverse"));
}

QColor ADeclarativeStyle::colorLatchedInverted() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-latched-inverse"));
}

QColor ADeclarativeStyle::colorHighlightedInverted() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-highlighted-inverse"));
}

QColor ADeclarativeStyle::colorDisabledLightInverted() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-disabled-light-inverse"));
}

QColor ADeclarativeStyle::colorDisabledMidInverted() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-disabled-mid-inverse"));
}

QColor ADeclarativeStyle::colorDisabledDarkInverted() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-disabled-dark-inverse"));
}

QColor ADeclarativeStyle::colorTextSelectionInverted() const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->colorParameter(QLatin1String("color-text-selection-inverse"));
}

QString ADeclarativeStyle::styleType () const
{
    Q_D(const ADeclarativeStyle);
    return d->engine->styleType ();
}

void ADeclarativeStyle::setStyleType (const QString &type)
{
    Q_D(const ADeclarativeStyle);
    d->engine->setStyleType (type);
}
