/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Components project.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

#define QT_NO_CAST_FROM_ASCII
#define QT_NO_CAST_TO_ASCII

#include "adeclarativestyleinternal.h"
#include "adeclarative.h"
#include "adeclarativescreen.h"
#include "astyleengine.h"

#include <QObject>
#include <QFileInfo>

#ifdef HAVE_MOBILITY
#include <QFeedbackEffect>
#endif //HAVE_MOBILITY


class ADeclarativeStyleInternalPrivate
{
public:
    ADeclarativeStyleInternalPrivate() {}

    AStyleEngine *engine;
};

ADeclarativeStyleInternal::ADeclarativeStyleInternal(AStyleEngine *engine, QObject *parent)
    : QObject(parent),
      d_ptr(new ADeclarativeStyleInternalPrivate())
{
    Q_D(ADeclarativeStyleInternal);
    d->engine = engine;
    QObject::connect(engine, SIGNAL(layoutParametersChanged()), this, SIGNAL(layoutParametersChanged()));
    QObject::connect(engine, SIGNAL(colorParametersChanged()), this, SIGNAL(colorParametersChanged()));
}

ADeclarativeStyleInternal::~ADeclarativeStyleInternal()
{
}

int ADeclarativeStyleInternal::statusBarHeight() const
{
    Q_D(const ADeclarativeStyleInternal);
    return d->engine->layoutParameter(QLatin1String("status-bar-height"));
}

int ADeclarativeStyleInternal::tabBarHeightPortrait() const
{
    Q_D(const ADeclarativeStyleInternal);
    return d->engine->layoutParameter(QLatin1String("tab-bar-height-portrait"));
}

int ADeclarativeStyleInternal::tabBarHeightLandscape() const
{
    Q_D(const ADeclarativeStyleInternal);
    return d->engine->layoutParameter(QLatin1String("tab-bar-height-landscape"));
}

int ADeclarativeStyleInternal::toolBarHeightPortrait() const
{
    Q_D(const ADeclarativeStyleInternal);
    return d->engine->layoutParameter(QLatin1String("tool-bar-height-portrait"));
}

int ADeclarativeStyleInternal::toolBarHeightLandscape() const
{
    Q_D(const ADeclarativeStyleInternal);
    return d->engine->layoutParameter(QLatin1String("tool-bar-height-landscape"));
}

int ADeclarativeStyleInternal::scrollBarThickness() const
{
    Q_D(const ADeclarativeStyleInternal);
    return d->engine->layoutParameter(QLatin1String("scroll-bar-thickness"));
}

int ADeclarativeStyleInternal::sliderThickness() const
{
    Q_D(const ADeclarativeStyleInternal);
    return d->engine->layoutParameter(QLatin1String("slider-thickness"));
}

int ADeclarativeStyleInternal::menuItemHeight() const
{
    Q_D(const ADeclarativeStyleInternal);
    return d->engine->layoutParameter(QLatin1String("menu-item-height"));
}

int ADeclarativeStyleInternal::dialogMinSize() const
{
    Q_D(const ADeclarativeStyleInternal);
    return d->engine->layoutParameter(QLatin1String("dialog-min-size"));
}

int ADeclarativeStyleInternal::dialogMaxSize() const
{
    Q_D(const ADeclarativeStyleInternal);
    return d->engine->layoutParameter(QLatin1String("dialog-max-size"));
}

int ADeclarativeStyleInternal::textFieldHeight() const
{
    Q_D(const ADeclarativeStyleInternal);
    return d->engine->layoutParameter(QLatin1String("text-field-height"));
}

int ADeclarativeStyleInternal::switchButtonHeight() const
{
    Q_D(const ADeclarativeStyleInternal);
    return d->engine->layoutParameter(QLatin1String("switch-button-height"));
}

int ADeclarativeStyleInternal::ratingIndicatorImageWidth() const
{
    Q_D(const ADeclarativeStyleInternal);
    return d->engine->layoutParameter(QLatin1String("rating-image-width"));
}

int ADeclarativeStyleInternal::ratingIndicatorImageHeight() const
{
    Q_D(const ADeclarativeStyleInternal);
    return d->engine->layoutParameter(QLatin1String("rating-image-height"));
}

int ADeclarativeStyleInternal::buttonSize() const
{
    Q_D(const ADeclarativeStyleInternal);
    return d->engine->layoutParameter(QLatin1String("button-size"));
}

void ADeclarativeStyleInternal::play(int effect)
{
#ifdef HAVE_MOBILITY
    QtMobility::QFeedbackEffect::playThemeEffect(static_cast<QtMobility::QFeedbackEffect::ThemeEffect>(effect));
#else
    Q_UNUSED(effect);
#endif //HAVE_MOBILITY
}

int ADeclarativeStyleInternal::textWidth(const QString &text, const QFont &font) const
{
    QFontMetrics metrics(font);
    return metrics.width(text) + 1; //Fix QTCOMPONENTS-810
}

int ADeclarativeStyleInternal::fontHeight(const QFont &font) const
{
    QFontMetrics metrics(font);
    return metrics.height();
}

QUrl ADeclarativeStyleInternal::toolBarIconPath(const QUrl &path, bool inverted) const
{
    if (!path.isEmpty()) {
        const QString scheme = path.scheme();
        const QFileInfo fileInfo = path.path();
        const QString completeBaseName = fileInfo.completeBaseName();

        if ((scheme.isEmpty() || scheme == QLatin1String("file") || scheme == QLatin1String("qrc"))
            && completeBaseName.startsWith(QLatin1String("toolbar-"))
            && completeBaseName.lastIndexOf(QLatin1Char('.')) == -1)
                return imagePath(completeBaseName, inverted);
    }
    return path;
}

QString ADeclarativeStyleInternal::imagePath(const QString &path, bool inverted) const
{
    QLatin1String invertedSuffix(inverted ? "_inverse" : "");
    return QLatin1String("image://theme/") + path + invertedSuffix;
}

bool ADeclarativeStyleInternal::isTabBarIcon(const QUrl &url) const
{
    if (url.isEmpty() || url.scheme() == QLatin1String("http"))
        return false;

    QFileInfo fileInfo = url.path();
    QString suffix = fileInfo.suffix();

    return suffix.toLower() == QLatin1String("svg") || suffix.isEmpty();
}
