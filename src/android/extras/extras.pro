include (../../../qt-components.pri)

TARGETPATH = com/nokia/android/extras.1.1
TEMPLATE = lib
TARGET = $$qtLibraryTarget(androidextrasplugin_1_1_2)
INCLUDEPATH += $$PWD

win32|mac:!wince*:!win32-msvc:!macx-xcode:CONFIG += debug_and_release build_all
CONFIG += qt plugin
QT += declarative

HEADERS += \
    adatetime.h

SOURCES += \
    plugin.cpp \
    adatetime.cpp

QML_FILES = \
    qmldir \
    Constants.js \
    DatePickerDialog.qml \
    InfoBanner.qml \
    RatingIndicator.qml \
    SearchBox.qml \
    TimePickerDialog.qml \
    Tumbler.js \
    Tumbler.qml \
    TumblerColumn.qml \
    TumblerDialog.qml \
    TumblerIndexHelper.js \
    TumblerTemplate.qml

include(../../../qml.pri)
