/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Components project.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/
#include "adeclarativemagnifier.h"
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QPainter>
#include <QDebug>
#include <QImageReader>
#include <QPixmapCache>

class ADeclarativeMagnifierPrivate
{

    Q_DECLARE_PUBLIC(ADeclarativeMagnifier)

public:
    ADeclarativeMagnifierPrivate(ADeclarativeMagnifier *qq):
        mScaleFactor(1), q_ptr(qq) {}
    ~ADeclarativeMagnifierPrivate() {}

    void init();
    void preparePixmaps();

    QRectF mSourceRect;
    qreal mScaleFactor;
    QPixmap mMask;
    QPixmap mOverlay;

    static QPixmap *mSource;
    static QString mOverlayFileName;
    static QString mMaskFileName;
    static const QString mMaskKey;
    static const QString mOverlayKey;

    ADeclarativeMagnifier *q_ptr;
};

QPixmap *ADeclarativeMagnifierPrivate::mSource = 0;
QString ADeclarativeMagnifierPrivate::mOverlayFileName = QString();
QString ADeclarativeMagnifierPrivate::mMaskFileName = QString();
const QString ADeclarativeMagnifierPrivate::mMaskKey = "sdmagnifierprivate_mmask";
const QString ADeclarativeMagnifierPrivate::mOverlayKey = "sdmagnifierprivate_moverlay";

void ADeclarativeMagnifierPrivate::init()
{
    Q_Q(ADeclarativeMagnifier);
    q->setFlag(QGraphicsItem::ItemHasNoContents, false);
}

void ADeclarativeMagnifierPrivate::preparePixmaps()
{
    QSize sourceSize = mSourceRect.size().toSize();

    if (mOverlay.size() != sourceSize
        && !mOverlayFileName.isEmpty()
        && (!QPixmapCache::find(mOverlayKey, &mOverlay) || mOverlay.size() != sourceSize))
    {
        QImageReader overlayReader(mOverlayFileName);
        overlayReader.setScaledSize(sourceSize);
        if (overlayReader.canRead())
            mOverlay = QPixmap::fromImage(overlayReader.read());
        QPixmapCache::insert(mOverlayKey, mOverlay);
    }

    if (mMask.size() != sourceSize
        && !mMaskFileName.isEmpty()
        && (!QPixmapCache::find(mMaskKey, &mMask) || mMask.size() != sourceSize))
    {
        QImageReader maskReader(mMaskFileName);
        maskReader.setScaledSize(sourceSize);
        if (maskReader.canRead())
            mMask = QPixmap::fromImage(maskReader.read());
        QPixmapCache::insert(mMaskKey, mMask);
    }

    if (!mSource || mSource->size() != sourceSize)
    {
        delete mSource;
        mSource = new QPixmap(sourceSize);
        mSource->fill(Qt::transparent);
    }
}

ADeclarativeMagnifier::ADeclarativeMagnifier(QDeclarativeItem *parent) :
    QDeclarativeItem(parent),
    d_ptr(new ADeclarativeMagnifierPrivate(this))
{
    Q_D(ADeclarativeMagnifier);
    d->init();
}

ADeclarativeMagnifier::~ADeclarativeMagnifier()
{
    Q_D(ADeclarativeMagnifier);
    delete d->mSource;
    d->mSource = 0;
}

void ADeclarativeMagnifier::setSourceRect(const QRectF &rect)
{
    Q_D(ADeclarativeMagnifier);

    if (rect != d->mSourceRect) {
        d->mSourceRect = rect;
        update();
        emit sourceRectChanged();
    }
}

QRectF ADeclarativeMagnifier::sourceRect() const
{
    Q_D(const ADeclarativeMagnifier);
    return d->mSourceRect;
}

void ADeclarativeMagnifier::setScaleFactor(qreal scaleFactor)
{
    Q_D(ADeclarativeMagnifier);

    if (scaleFactor != d->mScaleFactor) {
        d->mScaleFactor = scaleFactor;
        update();
        emit scaleFactorChanged();
    }
}

qreal ADeclarativeMagnifier::scaleFactor() const
{
    Q_D(const ADeclarativeMagnifier);

    return d->mScaleFactor;
}

void ADeclarativeMagnifier::setOverlayFileName(const QString &overlayFileName)
{
    Q_D(ADeclarativeMagnifier);

    if (overlayFileName != d->mOverlayFileName) {
        d->mOverlayFileName = overlayFileName;
        d->mOverlay = QPixmap();
        QPixmapCache::remove(d->mOverlayKey);
        update();
        emit overlayFileNameChanged();
    }
}

QString ADeclarativeMagnifier::overlayFileName() const
{
    Q_D(const ADeclarativeMagnifier);

    return d->mOverlayFileName;
}

void ADeclarativeMagnifier::setMaskFileName(const QString &maskFileName)
{
    Q_D(ADeclarativeMagnifier);

    if (maskFileName != d->mMaskFileName) {
        d->mMaskFileName = maskFileName;
        d->mMask = QPixmap();
        QPixmapCache::remove(d->mMaskKey);
        update();
        emit maskFileNameChanged();
    }
}

QString ADeclarativeMagnifier::maskFileName() const
{
    Q_D(const ADeclarativeMagnifier);

    return d->mMaskFileName;
}

void ADeclarativeMagnifier::paint(QPainter *painter, const QStyleOptionGraphicsItem *styleOption, QWidget *widget)
{
    Q_UNUSED(styleOption)
    Q_UNUSED(widget)

    Q_D(ADeclarativeMagnifier);

    // Cancel painting if source rectangle has not been set
    if (d->mSourceRect.size().toSize().isEmpty()) return;

    // Do not paint a magnifier inside a magnifier
    static bool inPaint = false;
    if (!inPaint) {
        inPaint = true;
        d->preparePixmaps();

        QPainter sourcePainter(d->mSource);
        QRectF targetRect = QRectF(QPointF(0, 0), d->mSourceRect.size());

        scene()->render(&sourcePainter, targetRect, d->mSourceRect);
        sourcePainter.setCompositionMode(QPainter::CompositionMode_DestinationIn);
        sourcePainter.drawPixmap(0, 0, d->mMask);
        sourcePainter.setCompositionMode(QPainter::CompositionMode_Multiply);
        sourcePainter.drawPixmap(0, 0, d->mOverlay);
        sourcePainter.end();

        painter->setRenderHint(QPainter::SmoothPixmapTransform);
        painter->drawPixmap(0, 0, boundingRect().width(), boundingRect().height(), *d->mSource);
        inPaint = false;
    }
}


