/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Components project.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

#include "adeclarative.h"
#include "aiconpool.h"
#include <QCoreApplication>
#include <QTime>
#include <QTimer>
#include <QDeclarativeContext>
#include <QDeclarativeEngine>
#include <QPixmapCache>
#include <QSysInfo>
#include <QDeclarativeItem>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>

#ifdef Q_OS_SYMBIAN
#include <AknUtils.h>
#include <e32std.h>
#include "stimeobserver.h"

#ifdef HAVE_SYMBIAN_INTERNAL
#include "ssharedstatusbarsubscriber.h"
#include <e32property.h>
#endif // HAVE_SYMBIAN_INTERNAL

#endif // Q_OS_SYMBIAN

#ifdef Q_OS_WIN
#include <windows.h>
#include <psapi.h>
#endif

//#define Q_DEBUG_SDECLARATIVE
#if defined(Q_DEBUG_SDECLARATIVE)
#include <QDebug>
#endif // Q_DEBUG_SDECLARATIVE

static const int MINUTE_MS = 60*1000;

#ifdef Q_OS_SYMBIAN
_LIT(KTimeFormat, "%J%:1%T");
#endif

class ADeclarativePrivate
#ifdef Q_OS_SYMBIAN
    : public MTimeChangeObserver
#endif
{
    Q_DECLARE_PUBLIC(ADeclarative)
public:
    ADeclarativePrivate(ADeclarative *qq)
        : q_ptr(qq)
        , mListInteractionMode(ADeclarative::TouchInteraction)
        , foreground(true)
        , rightToLeftDisplayLanguage(false)
        , graphicsSharing(false)
        , sharedStatusBar(false){
#ifdef Q_OS_SYMBIAN
        // Initialize based on the current UI language - it cannot be changed without a reboot.
        switch (User::Language()) {
            // These are the right-to-left UI languages supported in Symbian
            case ELangArabic:
            case ELangHebrew:
            case ELangFarsi:
            case ELangUrdu:
                rightToLeftDisplayLanguage = true;
                break;
            default:
                break;
        }
#ifdef HAVE_SYMBIAN_INTERNAL
        TInt handleIgnore(0);
        sharedStatusBar = RProperty::Get(KOffScreenSPaneUid, KOffScreenHandle, handleIgnore) == KErrNone;
#endif // HAVE_SYMBIAN_INTERNAL
        QT_TRAP_THROWING(timeChangeNotifier.reset(CTimeChangeObserver::NewL(this)));
#endif // Q_OS_SYMBIAN
    }

    ~ADeclarativePrivate()
    {
    }

    int allocatedMemory() const;
    void TimeChanged(); // overridden Symbian function
    ADeclarative *q_ptr;
    ADeclarative::InteractionMode mListInteractionMode;
    QTimer timer;
    bool foreground;
    bool rightToLeftDisplayLanguage;
    bool graphicsSharing;
    bool sharedStatusBar;
#ifdef Q_OS_SYMBIAN
    QScopedPointer<CTimeChangeObserver> timeChangeNotifier;
#endif
};

int ADeclarativePrivate::allocatedMemory() const
{
#if defined(Q_OS_SYMBIAN)
    TInt totalAllocated;
    User::AllocSize(totalAllocated);
    return totalAllocated;
#elif defined(Q_OS_WIN)
    PROCESS_MEMORY_COUNTERS pmc;
    if (!GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc)))
        return -1;
    return pmc.WorkingSetSize;
#else
    return -1;
#endif
}

void ADeclarativePrivate::TimeChanged()
{
    Q_Q(ADeclarative);
    emit q->currentTimeChanged();
}

ADeclarative::ADeclarative(QObject *parent) :
    QObject(parent)
{
    d_ptr.reset(new ADeclarativePrivate(this));
    d_ptr->timer.start(MINUTE_MS);
    connect(&d_ptr->timer, SIGNAL(timeout()), this, SIGNAL(currentTimeChanged()));

    QCoreApplication *application = QCoreApplication::instance();
    if (application)
        application->installEventFilter(this);
}

ADeclarative::~ADeclarative()
{
    Q_D(ADeclarative);
    d->timer.stop();
}

ADeclarative::InteractionMode ADeclarative::listInteractionMode() const
{
    Q_D(const ADeclarative);
    return d->mListInteractionMode;
}

void ADeclarative::setListInteractionMode(ADeclarative::InteractionMode mode)
{
    Q_D(ADeclarative);
    if (d->mListInteractionMode != mode) {
        d->mListInteractionMode = mode;
        emit listInteractionModeChanged();
    }
}

QString ADeclarative::currentTime()
{
#ifdef Q_OS_SYMBIAN
    TBuf<15> time;
    TTime homeTime;
    homeTime.HomeTime();
    TRAP_IGNORE(homeTime.FormatL(time, KTimeFormat));
    // Do the possible arabic indic digit etc. conversions
    AknTextUtils::DisplayTextLanguageSpecificNumberConversion(time);
    return QString(reinterpret_cast<const QChar *>(time.Ptr()), time.Length());
#else
    return QTime::currentTime().toString(QLatin1String("h:mm"));
#endif
}

bool ADeclarative::isForeground()
{
    Q_D(ADeclarative);
    return d->foreground;
}

int ADeclarative::privateAllocatedMemory() const
{
    Q_D(const ADeclarative);
    return d->allocatedMemory();
}

void ADeclarative::privateClearIconCaches()
{
    AIconPool::releaseAll();
    QPixmapCache::clear();
}

void ADeclarative::privateClearComponentCache()
{
    QDeclarativeContext *context = qobject_cast<QDeclarativeContext*>(this->parent());
    if (context)
        context->engine()->clearComponentCache();
}

ADeclarative::S60Version ADeclarative::s60Version() const
{
#ifdef Q_OS_SYMBIAN
    switch (QSysInfo::s60Version()) {
    case QSysInfo::SV_S60_5_2:
        return SV_S60_5_2;
#if QT_VERSION > 0x040703
    case QSysInfo::SV_S60_5_3:
        return SV_S60_5_3;
    case QSysInfo::SV_S60_5_4:
        return SV_S60_5_4;
#endif
    default:
        return SV_S60_UNKNOWN;
    }
#else
    return SV_S60_UNKNOWN;
#endif
}

bool ADeclarative::rightToLeftDisplayLanguage() const
{
    Q_D(const ADeclarative);
    return d->rightToLeftDisplayLanguage;
}

void ADeclarative::setGraphicsSharing(bool sharingEnabled)
{
    Q_D(ADeclarative);
    d->graphicsSharing = sharingEnabled;
}

bool ADeclarative::privateGraphicsSharing() const
{
    Q_D(const ADeclarative);
    return d->graphicsSharing;
}

void ADeclarative::privateSendMouseRelease(QDeclarativeItem *item) const
{
    // this is for situations where a press event opens another window (QWidget)
    // that eats the mouse released event. This method can be used for generating
    // the released event and 'correcting' the state of MouseArea/Flickable
    if (item) {
        QGraphicsSceneMouseEvent releaseEvent(QEvent::GraphicsSceneMouseRelease);
        item->scene()->sendEvent(item, &releaseEvent);
    }
}

bool ADeclarative::privateSharedStatusBar() const
{
    Q_D(const ADeclarative);
    return d->sharedStatusBar;
}

bool ADeclarative::eventFilter(QObject *obj, QEvent *event)
{
    Q_D(ADeclarative);
    if (obj == QCoreApplication::instance()) {
        if (event->type() == QEvent::ApplicationActivate) {
            emit currentTimeChanged();
#ifdef Q_OS_SYMBIAN
            d->timeChangeNotifier->StartObserving();
#endif
            d->timer.start(MINUTE_MS);
            d->foreground = true;
            emit foregroundChanged();
        } else if (event->type() == QEvent::ApplicationDeactivate) {
#ifdef Q_OS_SYMBIAN
            d->timeChangeNotifier->Cancel();
#endif
            d->timer.stop();
            d->foreground = false;
            emit foregroundChanged();
        }
    }
    return QObject::eventFilter(obj, event);
}
